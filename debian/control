Source: isc-kea
Section: net
Priority: optional
Maintainer: Kea Developers <kea-dev@lists.isc.org>
Uploaders: Kea Developers <kea-dev@lists.isc.org>
Build-Depends: bison,
               debhelper,
               default-libmysqlclient-dev | libmysqlclient-dev | libmariadb-dev-compat,
               dh-python,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               texlive,
               flex,
               libboost-dev,
               libboost-system-dev,
               liblog4cplus-dev,
               libpq-dev,
               libssl-dev,
               postgresql-server-dev-all,
               python3-dev
Standards-Version: 3.9.7
Homepage: http://kea.isc.org/
Vcs-Git: https://gitlab.isc.org/isc-projects/kea.git
Vcs-Browser: https://gitlab.isc.org/isc-projects/kea

Package: isc-kea
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: isc-kea-dhcp4 (= ${binary:Version}),
         isc-kea-dhcp6 (= ${binary:Version}),
         isc-kea-dhcp-ddns (= ${binary:Version}),
         isc-kea-ctrl-agent (= ${binary:Version}),
         isc-kea-admin (= ${binary:Version}),
         isc-kea-hooks (= ${binary:Version}),
         isc-kea-doc (= ${binary:Version}),
         ${misc:Depends}
Conflicts: kea
Description:
 Kea is a DHCPv4, DHCPv6, and DDNS server developed by the Internet Systems
 Consortium which provides very high-performance and supports PostgreSQL, MySQL
 and memfile backends.
 .
 This is a metapackage that provides the Kea DHCPv4, DHCPv6, and DHCP-DDNS
 servers, as well as the Kea Control Agent, Kea admin tools, and all of the
 basic Kea hooks.

Package: isc-kea-dhcp4-server
Depends: isc-kea-dhcp4 (>= 2.3.3-~), ${misc:Depends}
Architecture: any
Description: transitional package, do not remove
 This is an empty transitional package. Do not remove.

Package: isc-kea-dhcp6-server
Depends: isc-kea-dhcp6 (>= 2.3.3-~), ${misc:Depends}
Architecture: any
Description: transitional package, do not remove
 This is an empty transitional package. Do not remove.

Package: isc-kea-dhcp-ddns-server
Depends: isc-kea-dhcp-ddns (>= 2.3.3-~), ${misc:Depends}
Architecture: any
Description: transitional package, do not remove
 This is an empty transitional package. Do not remove.

Package: isc-kea-dhcp4
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: isc-kea-common (= ${binary:Version}),
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libcap2-bin [linux-any]
Conflicts: kea-dhcp4-server
Breaks: isc-kea-dhcp4-server (<< 2.3.3-~)
Replaces: isc-kea-dhcp4-server (<< 2.3.3-~)
Suggests: isc-kea-hooks,
          isc-kea-doc
Description: ISC Kea IPv4 DHCP server
 This package provides the Kea IPv4 DHCP server.

Package: isc-kea-dhcp6
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: isc-kea-common (= ${binary:Version}),
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libcap2-bin [linux-any]
Conflicts: kea-dhcp6-server
Breaks: isc-kea-dhcp6-server (<< 2.3.3-~)
Replaces: isc-kea-dhcp6-server (<< 2.3.3-~)
Suggests: isc-kea-hooks,
          isc-kea-doc
Description: ISC Kea IPv6 DHCP server
 This package provides the Kea IPv6 DHCP server.

Package: isc-kea-dhcp-ddns
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: isc-kea-common (= ${binary:Version}),
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: kea-dhcp-ddns-server
Breaks: isc-kea-dhcp-ddns-server (<< 2.3.3-~)
Replaces: isc-kea-dhcp-ddns-server (<< 2.3.3-~)
Suggests: isc-kea-doc
Description: ISC Kea DHCP Dynamic DNS service
 This package provides the Kea Dynamic DNS service to update DNS mapping based
 on DHCP lease events.

Package: isc-kea-ctrl-agent
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: isc-kea-common (= ${binary:Version}),
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: kea-ctrl-agent
Suggests: isc-kea-doc
Description: ISC Kea DHCP server REST API service
 This package provides the REST API service agent for Kea DHCP daemons.

Package: isc-kea-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Conflicts: kea-doc
Description: Documentation for ISC Kea DHCP server
 This package provides documentation for the Kea DHCP daemons and utilities.

Package: isc-kea-admin
Architecture: any
Section: admin
Depends: isc-kea-common (= ${binary:Version})
         ${misc:Depends}
Conflicts: kea-admin,
           python3-kea-connector
Breaks: python3-kea-connector,
        isc-kea-ctrl-agent (<< 2.3.3-~)
Replaces: python3-isc-kea-connector,
        isc-kea-ctrl-agent (<< 2.3.3-~)
Description: Administration utilities for ISC Kea DHCP server
 This package provides backend database initialization and migration
 scripts and the Kea shell for administering Kea via the Control Agent.

Package: isc-kea-perfdhcp
Architecture: any
Section: utils
Depends: ${misc:Depends},
         ${shlibs:Depends}
Conflicts: kea-perfdhcp
Description:
 This package provides a DHCP performance testing tool.

Package: isc-kea-dev
Architecture: any
Section: devel
Depends: isc-kea-common (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: kea-dev
Description: Development headers for ISC Kea DHCP server
 This package provides headers and static libraries of the common ISC Kea
 libraries, including libdhcp++

Package: isc-kea-common
Architecture: any
Section: libs
Depends: adduser,
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: kea-common
Description: Common libraries for the ISC Kea DHCP server
 This package provides common libraries and files used by ISC Kea servers and
 utilities.

Package: isc-kea-hooks
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description:
 This package contains a base set of hook libraries for Kea: high availability,
 bootp, flex_option, lease_cmds, stat_cmds, perfmon, and run_script.

Package: isc-kea-mysql
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: MySQL hook library for Kea
 This package contains the MySQL hook library for Kea.

Package: isc-kea-pgsql
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: PostgreSQL hook library for Kea
 This package contains the PostgreSQL hook library for Kea.

Package: isc-kea-premium-forensic-log
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Forensic Log library
 This package provides Forensic Log hook library for the Kea DHCP servers.

Package: isc-kea-premium-flex-id
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Flex ID hook library
 This package provides Flex ID hook library for the Kea DHCP servers.

Package: isc-kea-premium-host-cmds
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Host Commands hook library
 This package provides Host Commands hook library for the Kea DHCP servers.

Package: isc-kea-premium-subnet-cmds
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Subnet Commands hook library
 This package provides Subnet Commands hook library for the Kea DHCP servers.

Package: isc-kea-premium-radius
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version}),
Description: Kea Premium RADIUS hook library
 This package provides RADIUS hook library for the Kea DHCP servers.

Package: isc-kea-premium-host-cache
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Host Cache hook library
 This package provides Host Cache hook library for the Kea DHCP servers.

Package: isc-kea-premium-class-cmds
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Classification Commands hook library
 This package provides Classification Commands hook library for
 the Kea DHCP servers.

Package: isc-kea-premium-cb-cmds
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Config Backend Commands hook library
 This package provides Config Backend Commands hook library for
 the Kea DHCP servers.

Package: isc-kea-premium-lease-query
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium Lease Query hook library
 This package provides Lease Query hook library for the Kea DHCP servers.

Package: isc-kea-premium-gss-tsig
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp-ddns (= ${binary:Version})
Description: Kea Premium GSS-TSIG hook library
 This package provides GSS-TSIG hook library, intended to be loaded by the
 Kea DHCP-DDNS server. It provides GSS-API with Kerberos support and can be
 used to integrate with Windows Active Directory.

Package: isc-kea-premium-ddns-tuning
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium DDNS tunning hook library
 This package provides DDNS tuning hook library, intended to be loaded by Kea
 DHCPv4 or DHCPv6 servers. This hook library adds support for fine tuning
 various DNS update aspects.

Package: isc-kea-premium-limits
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version}) | isc-kea-dhcp6 (= ${binary:Version})
Description: Kea Premium limits hook library
 This package provides limits hook library, intended to be loaded by DHCPv4 or
 DHCPv6 servers. This hook library adds support for rate and lease limiting
 features.

Package: isc-kea-premium-rbac
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-ctrl-agent (= ${binary:Version})
Description: Kea Premium Role Based Access Control hook library
 This package provides role based access control hook library, intended to be
 loaded by Kea Control Agent. This hook library adds support for role based
 access control to Kea API.

Package: isc-kea-premium-ping-check
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends},
         isc-kea-dhcp4 (= ${binary:Version})
Description: Kea Premium Ping Check hook library
 With this hook library, kea-dhcp4 server can perform ping checks of candidate
 lease addresses before offering them to clients.