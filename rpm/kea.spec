#http://lists.fedoraproject.org/pipermail/devel/2011-August/155358.html
%global _hardened_build 1

#%%global prever beta

Name:           isc-kea
Version:        %{kea_version}
Release:        %{isc_version}%{?dist}
Summary:        DHCPv4, DHCPv6 and DDNS server from ISC

License:        MPLv2.0 and Boost
URL:            http://kea.isc.org
Source0:        https://downloads.isc.org/isc/kea/%{version}%{?prever:-%{prever}}/kea-%{version}%{?prever:-%{prever}}.tar.gz
Source1:        kea-dhcp4.service
Source2:        kea-dhcp6.service
Source3:        kea-dhcp-ddns.service
Source4:        kea-ctrl-agent.service

# these patches are no longer required, these things are already fixed
#Patch0:         0001-Fix-hooks-directory.patch
#Patch1:         0002-Fix-dns-pc-file.patch
#Patch2:         0003-Remove-spurious-fi.patch

# autoreconf
BuildRequires: autoconf automake libtool
BuildRequires: gcc-c++
# %%configure --with-openssl
BuildRequires: boost-devel
BuildRequires: openssl-devel
BuildRequires: mariadb-connector-c-devel
BuildRequires: libpq-devel
BuildRequires: log4cplus-devel
# Kea: this is not needed
#%ifnarch s390 %{mips}
#BuildRequires: valgrind-devel
#%endif
# src/lib/testutils/dhcp_test_lib.sh
BuildRequires: procps-ng
# %%configure --enable-generate-parser
#BuildRequires: bison  # not required
#BuildRequires: flex  # not required
# %%configure --enable-shell
BuildRequires: python3-devel
# %%configure --with-gssapi
# in case you ever wanted to use %%configure --enable-generate-docs
%if 0%{?centos} < 7 && 0%{?rhel} < 7
BuildRequires: python3-sphinx
BuildRequires: texlive
BuildRequires: texlive-collection-latexextra
%endif
BuildRequires: systemd

Requires: isc-kea-admin%{?_isa} = %{version}-%{release}
Requires: isc-kea-ctrl-agent%{?_isa} = %{version}-%{release}
Requires: isc-kea-dhcp-ddns%{?_isa} = %{version}-%{release}
Requires: isc-kea-dhcp4%{?_isa} = %{version}-%{release}
Requires: isc-kea-dhcp6%{?_isa} = %{version}-%{release}
Requires: isc-kea-hooks%{?_isa} = %{version}-%{release}
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Conflicts: kea
Obsoletes: kea
%description
DHCP implementation from Internet Systems Consortium, Inc. that features fully
functional DHCPv4, DHCPv6 and Dynamic DNS servers.
Both DHCP servers fully support server discovery, address assignment, renewal,
rebinding and release. The DHCPv6 server supports prefix delegation. Both
servers support DNS Update mechanism, using stand-alone DDNS daemon.


%package doc
Summary: DHCPv4 and DHCPv6 server from ISC (documentation)
%description doc
Documentation for the Kea DHCP server suite

%package devel
Summary: Development headers and libraries for Kea DHCP server
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
# to build hooks (#1335900)
BuildRequires: boost-devel
BuildRequires: openssl-devel
Requires: pkgconfig
Conflicts: kea-devel
Obsoletes: kea-devel
%description devel
Header files and API documentation.

%package common
Summary: Kea shared libraries, LFC, and runstate files
Summary: Shared libraries used by Kea DHCP server
Conflicts: kea-libs
Obsoletes: kea-libs
Obsoletes: isc-kea-libs
%description common
Contains the Kea shared libraries, the Lease File Cleanup script, and various
things that are neededby the services.


%package admin
Summary: Database administration tools for Kea DHCP server
# to build hooks (#1335900)
Requires: python3
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
Obsoletes: isc-kea-shell
%description admin
To manage the databases, Kea provides the kea-admin tool. It can initialize a
new backend, check its version number, perform a backend upgrade, and dump
lease data to a text file.
This package also contains the Kea shell, which provides a way to communicate
with the Kea Control Agent from the CLI. It is a simple command-line,
scripting-friendly, text client that is able to connect to the CA, send it
commands with parameters, retrieve theresponses, and display them.

%package dhcp-ddns
Summary: Kea DHCP Dynamic DNS Server
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description dhcp-ddns
The Kea DHCP-DDNS Server (kea-dhcp-ddns, known informally as D2) conducts the
client side of the Dynamic DNS protocol (DDNS, defined in RFC 2136) on behalf
of the DHCPv4 and DHCPv6 servers (kea-dhcp4 and kea-dhcp6 respectively). The
DHCP servers construct DDNS update requests, known as NameChangeRequests
(NCRs), based on DHCP lease change events and then post them to D2. D2 attempts
to match each request to the appropriate DNS server(s) and carries out the
necessary conversation with those servers to update the DNS data.

%package dhcp4
Summary: Kea IPv4 DHCP Server
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description dhcp4
The Kea DHCPv4 Server

%package dhcp6
Summary: Kea IPv6 DHCP Server
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description dhcp6
The Kea DHCPv6 Server

%package ctrl-agent
Summary: Kea Control Agent - REST service for controlling Kea DHCP server
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description ctrl-agent
The Kea Control Agent (CA) is a daemon which exposes a RESTful control
interface for managing Kea servers. The daemon can receive control commands
over HTTP and either forward these commands to the respective Kea servers or
handle these commands on its own.

%package perfdhcp
Summary: Kea Optional Utils - perfdhcp
%description perfdhcp
perfdhcp is a DHCP benchmarking tool. It provides a way to measure the
performance of DHCP servers by generating large amounts of traffic from
multiple simulated clients. It is able to test both IPv4 and IPv6 servers, and
provides statistics concerning response times and the number of requests that
are dropped.

%package hooks
Summary: Open source hook libraries for Kea
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
Conflicts: kea-hooks
Obsoletes: kea-hooks
%description hooks
Hooking mechanism allow Kea to load one or more dynamically-linked libraries
(known as "hooks libraries") and, at various points in its processing
("hook points"), call functions in them.  Those functions perform whatever
custom processing is required. This package contains the following hooks:
ha (high availability), bootp, stat-cmds, run-script,
lease-cmds, flex-option, perfmon.

%package mysql
Summary: Open source MySQL hook library for Kea
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description mysql
This package contains the MySQL hook library for Kea.

%package pgsql
Summary: Open source PostgreSQL hook library for Kea
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description pgsql
This package contains the PostgreSQL hook library for Kea.

%package premium-forensic-log
Summary: Kea Premium Forensic Log hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-forensic-log
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Forensic Log hook library.

%package premium-flex-id
Summary: Kea Premium Flex ID hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-flex-id
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Flex ID hook library.

%package premium-host-cmds
Summary: Kea Premium Host Commands hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-host-cmds
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Host Commands hook library.

%package premium-subnet-cmds
Summary: Kea Premium Subnet Commands hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-subnet-cmds
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Subnet Commands hook library.

%package premium-radius
Summary: Kea Premium RADIUS hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-radius
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides new RADIUS hook library.

%package premium-host-cache
Summary: Kea Premium Host Cache hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-host-cache
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Host Cache hook library.

%package premium-class-cmds
Summary: Kea Premium Classification Commands hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-class-cmds
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Classification Commands hook library.

%package premium-cb-cmds
Summary: Kea Premium Config Backend Commands hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-cb-cmds
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Config Backend Commands hook library.

%package premium-lease-query
Summary: Kea Premium Leasequery hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-lease-query
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides Lease Query hook library.

%package premium-gss-tsig
Summary: Kea Premium GSS-TSIG hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-gss-tsig
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides GSS-TSIG hook library, intended to be loaded by DHCP-DDNS server.
It provides GSS-API with Kerberos support and can be used to integrate with Windows Active Directory.

%package premium-ddns-tuning
Summary: Kea Premium DDNS tuning hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-ddns-tuning
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides DDNS tuning hook library, intended to be loaded by DHCPv4 or DHCPv6 servers.
This hook library adds support for fine tuning various DNS update aspects.

%package premium-limits
Summary: Kea Premium limits hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-limits
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides limits hook library, intended to be loaded by DHCPv4 or DHCPv6 servers.
This hook library adds support for rate and lease limiting features.

%package premium-rbac
Summary: Kea Premium Role Based Access Control hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-rbac
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides role based access control hook library, intended to be loaded by Kea Control Agent.
This hook library adds support for role based access control to Kea API.

%package premium-ping-check
Summary: Kea Premium Ping Check hook library
Requires(pre): isc-kea-common%{?_isa} = %{version}-%{release}
Requires: isc-kea-common%{?_isa} = %{version}-%{release}
%description premium-ping-check
Kea is an IPv4 and IPv6 DHCP server developed by Internet Systems Consortium.
This package provides DDNS tuning hook library, intended to be loaded by DHCPv4 server.
This hook library adds support for performing ping checks of candidate lease addresses before offering them to clients.

%prep
%autosetup -p1 -n kea-%{version}%{?prever:-%{prever}}

# to be able to build on ppc64(le)
# https://sourceforge.net/p/flex/bugs/197
# https://lists.isc.org/pipermail/kea-dev/2016-January/000599.html
sed -i -e 's|ECHO|YYECHO|g' src/lib/eval/lexer.cc

# replace -flto=auto -ffat-lto-objects with -fno-lto in cxx flags
%define _lto_cflags -fno-lto

%build
autoreconf --verbose --force --install
export CXXFLAGS="%{optflags}"
export LDFLAGS="%{build_ldflags} -fno-lto"

%if 0%{?centos} >= 7 || 0%{?rhel} >= 7
%define with_sphinx --with-sphinx=$HOME/venv/bin/sphinx-build
%else
%define with_sphinx ''
%endif
export KEA_PKG_VERSION_IN_CONFIGURE=%{release}
export KEA_PKG_TYPE_IN_CONFIGURE="rpm"

%configure \
    --disable-dependency-tracking \
    --disable-rpath \
    --disable-silent-rules \
    --disable-static \
    --enable-generate-docs \
    --enable-generate-messages \
    --enable-shell \
    --enable-perfdhcp \
    --with-mysql \
    --with-pgsql \
    --with-gnu-ld \
    --with-log4cplus \
    --with-openssl \
    --with-gssapi \
    %{with_sphinx}

%make_build


%install
%make_install docdir=%{_pkgdocdir}

# Get rid of .la files
find %{buildroot} -type f -name "*.la" -delete -print

# remove keactrl
rm %{buildroot}%{_sysconfdir}/kea/keactrl.conf
rm %{buildroot}%{_sbindir}/keactrl
rm %{buildroot}%{_mandir}/man8/keactrl.8

# remove netconf files
rm %{buildroot}%{_mandir}/man8/kea-netconf.8

# Install systemd units
install -Dpm 0644 %{S:1} %{buildroot}%{_unitdir}/kea-dhcp4.service
install -Dpm 0644 %{S:2} %{buildroot}%{_unitdir}/kea-dhcp6.service
install -Dpm 0644 %{S:3} %{buildroot}%{_unitdir}/kea-dhcp-ddns.service
install -Dpm 0644 %{S:4} %{buildroot}%{_unitdir}/kea-ctrl-agent.service

# Start empty lease databases
mkdir -p %{buildroot}%{_sharedstatedir}/kea/
touch %{buildroot}%{_sharedstatedir}/kea/kea-leases4.csv
touch %{buildroot}%{_sharedstatedir}/kea/kea-leases6.csv

rm -f %{buildroot}%{_pkgdocdir}/COPYING

mkdir -p %{buildroot}%{_localstatedir}/run
install -d -m 0755 %{buildroot}%{_localstatedir}/run/kea/

mkdir -p %{buildroot}%{_localstatedir}/log
install -d -m 0755 %{buildroot}%{_localstatedir}/log/kea/

# install /usr/lib/tmpfiles.d/kea.conf
mkdir -p %{buildroot}%{_tmpfilesdir}
cat > %{buildroot}%{_tmpfilesdir}/kea.conf <<EOF
# kea needs existing /run/kea/ to create logger_lockfile and pidfile there
# See tmpfiles.d(5) for details

d /run/kea 0755 kea kea -
EOF

# change log destination from /var/log/... to STDOUT and enable shortened log format
sed -i -e s/\"output\".*/\"output\":\ \"stdout\",/ -e s@\/\/\ \"pattern@\"pattern@ \
    %{buildroot}%{_sysconfdir}/kea/kea-ctrl-agent.conf \
    %{buildroot}%{_sysconfdir}/kea/kea-dhcp6.conf \
    %{buildroot}%{_sysconfdir}/kea/kea-dhcp4.conf \
    %{buildroot}%{_sysconfdir}/kea/kea-dhcp-ddns.conf
#    %{buildroot}%{_sysconfdir}/kea/kea-netconf.conf  # TODO: no support for netconf/sysconf yet


%post
%systemd_post kea-dhcp4.service kea-dhcp6.service kea-dhcp-ddns.service kea-ctrl-agent.service

%preun
%systemd_preun kea-dhcp4.service kea-dhcp6.service kea-dhcp-ddns.service kea-ctrl-agent.service

%postun
%systemd_postun_with_restart kea-dhcp4.service kea-dhcp6.service kea-dhcp-ddns.service kea-ctrl-agent.service

%pre common
getent group kea >/dev/null || groupadd -r kea
getent passwd kea >/dev/null || \
    useradd -l -r -g kea -d /var/lib/kea -s /sbin/nologin \
    -c "ISC Kea DHCP service user" kea
exit 0

%if 0%{?rhel} == 7 || 0%{?centos} == 7
%post common -p /sbin/ldconfig
%postun common -p /sbin/ldconfig
%else
%ldconfig_scriptlets common
%endif


%files
%license COPYING

%files perfdhcp
%{_sbindir}/perfdhcp
%{_mandir}/man8/perfdhcp.8.gz

%files admin
%{_sbindir}/kea-admin
%{_mandir}/man8/kea-admin.8.gz
%dir %{_datarootdir}/kea/
%{_datarootdir}/kea/api
%{_datarootdir}/kea/scripts
%{_sbindir}/kea-shell
%{_mandir}/man8/kea-shell.8.gz
%{python3_sitelib}/kea/kea_conn.py
%{python3_sitelib}/kea/kea_connector3.py
%{python3_sitelib}/kea/__pycache__/kea_conn.cpython-%{python3_version_nodots}.opt-1.pyc
%{python3_sitelib}/kea/__pycache__/kea_conn.cpython-%{python3_version_nodots}.pyc
%{python3_sitelib}/kea/__pycache__/kea_connector3.cpython-%{python3_version_nodots}.opt-1.pyc
%{python3_sitelib}/kea/__pycache__/kea_connector3.cpython-%{python3_version_nodots}.pyc

#%files netconf
#%{_datarootdir}/kea/yang  # TODO: no support for netconf/sysconf yet
#%config(noreplace) %{_sysconfdir}/kea/kea-netconf.conf  # TODO: no support for netconf/sysconf yet

%files ctrl-agent
%{_sbindir}/kea-ctrl-agent
%{_unitdir}/kea-ctrl-agent.service
%{_mandir}/man8/kea-ctrl-agent.8.gz
%attr(0640,kea,kea) %config(noreplace) %{_sysconfdir}/kea/kea-ctrl-agent.conf

%files dhcp-ddns
%caps(cap_net_bind_service+ep) %{_sbindir}/kea-dhcp-ddns
%{_unitdir}/kea-dhcp-ddns.service
%{_mandir}/man8/kea-dhcp-ddns.8.gz
%attr(0640,kea,kea) %config(noreplace) %{_sysconfdir}/kea/kea-dhcp-ddns.conf

%files dhcp4
%caps(cap_net_bind_service,cap_net_raw+ep) %{_sbindir}/kea-dhcp4
%{_unitdir}/kea-dhcp4.service
%{_mandir}/man8/kea-dhcp4.8.gz
%attr(0640,kea,kea) %config(noreplace) %{_sysconfdir}/kea/kea-dhcp4.conf
%attr(0640,kea,kea) %config(noreplace) %{_sharedstatedir}/kea/kea-leases4.csv

%files dhcp6
%caps(cap_net_bind_service+ep) %{_sbindir}/kea-dhcp6
%{_unitdir}/kea-dhcp6.service
%{_mandir}/man8/kea-dhcp6.8.gz
%attr(0640,kea,kea) %config(noreplace) %{_sysconfdir}/kea/kea-dhcp6.conf
%attr(0640,kea,kea) %config(noreplace) %{_sharedstatedir}/kea/kea-leases6.csv

%files doc
%{_pkgdocdir}/AUTHORS
%{_pkgdocdir}/ChangeLog
%{_pkgdocdir}/code_of_conduct.md
%{_pkgdocdir}/CONTRIBUTING.md
%{_pkgdocdir}/examples
%{_pkgdocdir}/html
%{_pkgdocdir}/platforms.rst
%{_pkgdocdir}/README
%{_pkgdocdir}/SECURITY.md
%if 0%{?centos} < 7 && 0%{?rhel} < 7
%{_pkgdocdir}/kea-arm.pdf
%{_pkgdocdir}/kea-messages.pdf
%endif

%files devel
# TODO: list of header files should be put here explicitly
%license COPYING
%{_bindir}/kea-msg-compiler
%{_includedir}/kea
%{_libdir}/libkea-asiodns.so
%{_libdir}/libkea-asiolink.so
%{_libdir}/libkea-cc.so
%{_libdir}/libkea-cfgclient.so
%{_libdir}/libkea-cryptolink.so
%{_libdir}/libkea-d2srv.so
%{_libdir}/libkea-database.so
%{_libdir}/libkea-dhcp++.so
%{_libdir}/libkea-dhcp_ddns.so
%{_libdir}/libkea-dhcpsrv.so
%{_libdir}/libkea-dns++.so
%{_libdir}/libkea-eval.so
%{_libdir}/libkea-exceptions.so
%{_libdir}/libkea-hooks.so
%{_libdir}/libkea-http.so
%{_libdir}/libkea-log.so
%{_libdir}/libkea-process.so
%{_libdir}/libkea-stats.so
%{_libdir}/libkea-util-io.so
%{_libdir}/libkea-util.so
%{_libdir}/libkea-tcp.so

%files common
%license COPYING
%{_sbindir}/kea-lfc
%dir %{_sysconfdir}/kea/
%attr(0750,kea,kea) %dir %{_sharedstatedir}/kea
%{_mandir}/man8/kea-lfc.8.gz
%attr(0750,kea,kea) %dir %{_localstatedir}/run/kea/
%attr(0750,kea,kea) %dir %{_localstatedir}/log/kea/
%{_tmpfilesdir}/kea.conf
%{_libdir}/libkea-asiodns.so.*
%{_libdir}/libkea-asiolink.so.*
%{_libdir}/libkea-cc.so.*
%{_libdir}/libkea-cfgclient.so.*
%{_libdir}/libkea-cryptolink.so.*
%{_libdir}/libkea-d2srv.so.*
%{_libdir}/libkea-database.so.*
%{_libdir}/libkea-dhcp++.so.*
%{_libdir}/libkea-dhcp_ddns.so.*
%{_libdir}/libkea-dhcpsrv.so.*
%{_libdir}/libkea-dns++.so.*
%{_libdir}/libkea-eval.so.*
%{_libdir}/libkea-exceptions.so.*
%{_libdir}/libkea-hooks.so.*
%{_libdir}/libkea-http.so.*
%{_libdir}/libkea-log.so.*
%{_libdir}/libkea-process.so.*
%{_libdir}/libkea-stats.so.*
%{_libdir}/libkea-util-io.so.*
%{_libdir}/libkea-util.so.*
%{_libdir}/libkea-tcp.so.*

# Open Source Hooks
%files hooks
%{_libdir}/kea/hooks/libdhcp_bootp.so
%{_libdir}/kea/hooks/libdhcp_flex_option.so
%{_libdir}/kea/hooks/libdhcp_ha.so
%{_libdir}/kea/hooks/libdhcp_lease_cmds.so
%{_libdir}/kea/hooks/libdhcp_perfmon.so
%{_libdir}/kea/hooks/libdhcp_run_script.so
%{_libdir}/kea/hooks/libdhcp_stat_cmds.so

%files mysql
%{_libdir}/libkea-mysql.so*
%{_libdir}/kea/hooks/libdhcp_mysql.so

%files pgsql
%{_libdir}/libkea-pgsql.so*
%{_libdir}/kea/hooks/libdhcp_pgsql.so

# Premium Hooks
%files premium-forensic-log
%{_libdir}/kea/hooks/libdhcp_legal_log.so
%files premium-flex-id
%{_libdir}/kea/hooks/libdhcp_flex_id.so
%files premium-host-cmds
%{_libdir}/kea/hooks/libdhcp_host_cmds.so
%files premium-subnet-cmds
%{_libdir}/kea/hooks/libdhcp_subnet_cmds.so
%files premium-radius
%{_libdir}/kea/hooks/libdhcp_radius.so
%dir %{_sysconfdir}/kea/radius
%{_sysconfdir}/kea/radius/dictionary
%files premium-host-cache
%{_libdir}/kea/hooks/libdhcp_host_cache.so
%files premium-class-cmds
%{_libdir}/kea/hooks/libdhcp_class_cmds.so
%files premium-cb-cmds
%{_libdir}/kea/hooks/libdhcp_cb_cmds.so
%files premium-lease-query
%{_libdir}/kea/hooks/libdhcp_lease_query.so
%files premium-gss-tsig
%{_libdir}/kea/hooks/libddns_gss_tsig.so
%files premium-ddns-tuning
%{_libdir}/kea/hooks/libdhcp_ddns_tuning.so
%files premium-limits
%{_libdir}/kea/hooks/libdhcp_limits.so
%files premium-rbac
%{_libdir}/kea/hooks/libca_rbac.so
%{_libdir}/kea/hooks/libdhcp_rbac.so
%files premium-ping-check
%{_libdir}/kea/hooks/libdhcp_ping_check.so

%changelog
* Tue Feb 4 2025 Wlodek Wencel <wlodek@isc.org> - %{version}-%{release}
- Add libpq-devel instead of postgresql-server-devel as build requirement.

* Mon Oct 7 2024 Andrei Pavel <andrei@isc.org> - 2.7.4-isc20241029142217
- Remove the CB, HB, LB hook libraries from the common package and add the
  generic MySQL and PostgreSQL hook libraries in their own packages.
- Remove flag '-std=gnu++11'.

* Tue Sep 17 2024 Marcin Godzina <mgodzina@isc.org> - 2.7.3-isc20240923075655
- Added database libs to common package.

* Tue Aug 13 2024 Andrei Pavel <andrei@isc.org> - 2.7.2-isc20240827110548
- Stop useradd from logging login information for the kea user which resulted
  in a non-fatal error starting with Fedora 40.
- Add libdhcp_rbac.so to packages.

* Tue May 14 2024 Andrei Pavel <andrei@isc.org> - 2.6.0-isc20240525133310
- Fix a pidfile error caused by /run/kea being owned by root.

* Mon Feb 19 2024 Wlodek Wencel <wlodek@isc.org> - 2.5.6-isc20240226130228
- Added new hook lib perfmon into hooks package

* Fri Dec 1 2023 Wlodek Wencel <wlodek@isc.org> - 2.5.5-isc20240129145054
- Removed old radius hook and freeradius dependency

* Mon Oct 23 2023 Wlodek Wencel <wlodek@isc.org> - 2.5.3-isc20231024153937
- Added SECURITY.md file
- -V option will return exact package version instead of tarball

* Tue Sep 26 2023 Marcin Godzina <mgodzina@isc.org> - 2.5.2-isc20230926095551
- New ping check hook introduced.

* Mon Sep 25 2023 Wlodek Wencel <wlodek@isc.org> - 2.5.2-isc20230926095551
- new radius hook introduced to replace old, old was move to old_radius

* Wed Sep 20 2023 Wlodek Wencel <wlodek@isc.org> - 2.4.0-isc20230921141113
- changed privileges on installed config files
- added no-lto cxx flag

* Thu Jun 29 2023 Wlodek Wencel <wlodek@isc.org> - 2.4.0-isc20230630120747
- remove python2 files procedure

* Wed Apr 19 2023 Wlodek Wencel <wlodek@isc.org> - 2.3.7-isc20230421110852
- --with-pgsql option usage changed for rhel 9

* Mon Nov 14 2022 Wlodek Wencel <wlodek@isc.org> - 2.3.3-isc20221126190304
- New libkea-tcp added to packages

* Tue Oct 4 2022 Dan Theisen <djt@isc.org> - 2.3.2-isc20221026061952
- Run Kea as 'kea' user, change permissions of relevent files

* Mon Sep 19 2022 Dan Theisen <djt@isc.org> - 2.3.1-isc20220928105532
- Split packages up more granularly to standardize packages across distros

* Mon Sep 12 2022 Wlodek Wencel <wlodek@isc.org> - 2.3.1-isc20220928105532
- Remove dhcp4 dhcp6 ddns processes from Control Agent WantedBy statements

* Tue Jun 14 2022 Wlodek Wencel <wlodek@isc.org> - 2.1.7
- Add rhel 9 support

* Fri May 20 2022 Wlodek Wencel <wlodek@isc.org> - 2.1.6
- Add new limits and rbac premium hook

* Wed Apr 13 2022 Wlodek Wencel <wlodek@isc.org> - 2.1.5
- Add new ddns tuning premium hook

* Wed May 22 2019 Felix Kaechele <heffer@fedoraproject.org> - 1.5.0-4
- Update to 1.3.0 release version
- fix PID file path in service files
- clean up spec file
- switched to openssl-devel, now builds with openssl 1.1
- install systemd units manually instead of patching the souce to do it
- enable kea-shell
- add boost patch
- add kea-ctrl-agent unit
- change postgresql-devel to postgresql-server-devel
- update to 1.4.0

* Sun Dec 16 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.5.0-3
- Update to released version

* Tue Dec 11 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.5.0-beta2.2%{?dist}
- Do not require -connectors on RHEL

* Tue Dec  4 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.5.0-beta2.1%{?dist}
- update to beta2

* Tue Nov 20 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.5.0-2
- Update to 1.5.0 beta

* Mon Aug 27 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.3.0-12
- Disable tests again.

* Mon Aug 27 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.3.0-11
- Do not use compat verion of openssl

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.0-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Thu May 17 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.3.0-9
- Fix config files names (#1579298)

* Mon Feb 19 2018 Pavel Zhukov <pzhukov@redhat.com> - 1.3.0-8
- Add gcc-c++ BR

* Wed Feb 14 2018 Pavel Zhukov <landgraf@fedoraproject.org> - 1.3.0-7
- Package released version (#1545096)

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jan 31 2018 Pavel Zhukov <landgraf@fedoraproject.org> - 1.3.0-4
- Fix build with boost 1.66 (#1540331)

* Thu Nov  2 2017 Pavel Zhukov <pzhukov@redhat.com> - 1.3.0-3
- Add openssl-devel requires
- Do not force pkgconfig(openssl) version

* Mon Oct 23 2017 Pavel Zhukov <pzhukov@redhat.com> - 1.2.0-8
- Require openssl102

* Sun Oct 22 2017 Pavel Zhukov <pzhukov@redhat.com> - 1.2.0-7
- Rebuild with new openssl

* Thu Oct 12 2017 Pavel Zhukov <pzhukov@redhat.com> - 1.2.0-6
- Use mariadb-connector-c-devel instead of mysql-devel (#1493628)

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jul 03 2017 Jonathan Wakely <jwakely@redhat.com> - 1.2.0-3
- Rebuilt for Boost 1.64

* Fri May 26 2017 Pavel Zhukov <landgraf@fedoraproject.org> - 1.2.0-2
- New release 1.2.0 (#1440348)

* Tue Apr 04 2017 Pavel Zhukov <landgraf@fedoraproject.org> - 1.1.0-3
- Add patch for OpenSSL 1.1. Fix FTBFS (#1423812)

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Oct 04 2016 Jiri Popelka <jpopelka@redhat.com> - 1.1.0-1
- 1.1.0

* Thu Sep 01 2016 Jiri Popelka <jpopelka@redhat.com> - 1.1.0-0.1
- 1.1.0-beta

* Fri Aug 12 2016 Michal Toman <mtoman@fedoraproject.org> - 1.0.0-11
- No valgrind on MIPS

* Wed Aug 03 2016 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-10
- %%{_defaultdocdir}/kea/ -> %%{_pkgdocdir}

* Fri May 13 2016 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-9
- devel subpackage Requires: boost-devel

* Wed Mar 23 2016 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-8
- Rebuild for log4cplus-1.2.0-2

* Wed Mar 23 2016 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-7
- Rebuilding kea for log4cplus-1.2.0

* Wed Mar 16 2016 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-6
- Editing pgsql_lease_mgr.cc according to upstream

* Fri Mar 11 2016 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-4
- Fixing bugs created from new C++ standard

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jan 15 2016 Jonathan Wakely <jwakely@redhat.com> - 1.0.0-2
- Rebuilt for Boost 1.60

* Tue Dec 29 2015 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-1
- 1.0.0

* Wed Dec 23 2015 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-0.3.beta2
- fix compile error

* Wed Dec 23 2015 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-0.2.beta2
- 1.0.0-beta2

* Wed Dec 09 2015 Jiri Popelka <jpopelka@redhat.com> - 1.0.0-0.1.beta
- 1.0.0-beta

* Mon Aug 24 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.2-3
- fix valgrind-devel availability

* Wed Jul 29 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.2-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/F23Boost159

* Tue Jul 28 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.2-1
- 0.9.2

* Wed Jul 22 2015 David Tardon <dtardon@redhat.com> - 0.9.2-0.2.beta
- rebuild for Boost 1.58

* Thu Jul 02 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.2-0.1.beta
- 0.9.2-beta

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 0.9.1-2
- Rebuilt for GCC 5 C++11 ABI change

* Wed Apr 01 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.1-1
- 0.9.1

* Fri Feb 20 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.1-0.2.beta
- /run/kea/ (for logger_lockfile)

* Thu Feb 19 2015 Jiri Popelka <jpopelka@redhat.com> - 0.9.1-0.1.beta
- 0.9.1-beta

* Tue Jan 27 2015 Petr Machata <pmachata@redhat.com> - 0.9-4
- Rebuild for boost 1.57.0

* Tue Nov 04 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-3
- do not override @localstatedir@ globally
- include latest upstream kea.conf

* Wed Sep 24 2014 Dan Horák <dan[at]danny.cz> - 0.9-2
- valgrind available only on selected arches

* Mon Sep 01 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-1
- 0.9

* Thu Aug 21 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-0.5.beta1
- fix building with PostgreSQL on i686
- redefine localstatedir to sharedstatedir (kea#3523)

* Wed Aug 20 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-0.4.beta1
- install systemd service units with a proper patch that we can send upstream
- build with MySQL & PostgreSQL & Google Test
- no need to copy sample configuration, /etc/kea/kea.conf already contains one

* Tue Aug 19 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-0.3.beta1
- comment patches
- use --preserve-timestamps with install

* Mon Aug 18 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-0.2.beta1
- make it build on armv7
- BuildRequires procps-ng for %%check
- use install instead of cp
- configure.ac: AC_PROG_LIBTOOL -> LT_INIT
- move license files to -libs

* Thu Aug 14 2014 Jiri Popelka <jpopelka@redhat.com> - 0.9-0.1.beta1
- initial spec
